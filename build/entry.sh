#!/bin/sh
set -e

/usr/bin/rebar get-deps compile

exec "$@"

