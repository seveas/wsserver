IMAGE=leanlabs/wsserver
TAG=0.0.2

help:
	@echo "here will be drief doc"

build:
	@docker run --rm -v $(CURDIR):/data leanlabs/erlang-builder rebar get-deps compile generate

release:
	@docker build -t $(IMAGE) .
	@docker tag $(IMAGE):latest $(IMAGE):$(TAG)
	@docker push $(IMAGE):latest
	@docker push $(IMAGE):$(TAG)

.PHONY: help build release
